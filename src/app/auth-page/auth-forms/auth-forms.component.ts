import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth-page/auth.service';
import { IUserInput, IAlert } from 'src/app/interfaces';

@Component({
  selector: 'app-auth-forms',
  templateUrl: './auth-forms.component.html',
  styleUrls: ['./auth-forms.component.css'],
})
export class AuthFormsComponent implements OnInit {
  public authForm: FormGroup;
  public alerts: IAlert[];

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.authForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  onSignin(): void {
    const user: IUserInput = this.authForm.value;
    this.authService.signin(user);
  }
}
