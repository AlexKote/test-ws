import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { IUserInput, IUserAuth } from '../interfaces';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user: IUserAuth;

  constructor(private http: HttpClient, private router: Router) {}

  signin(user: IUserInput): void {
    const url = 'http://dev.angulartest.digital-era.ru/api/login';
    this.http.post(url, user).subscribe(
      (res: IUserAuth) => {
        this.user = res;
        this.router.navigate(['/data']);
      },
      (err) => {
        console.log(err);
        alert(err.error.error);
      }
    );
  }

  logout(): void {
    this.user = null;
  }

  getToken(): string {
    return this.user.token;
  }

  isAuthenticated(): boolean {
    return this.user != null;
  }

  isAdmin(): boolean {
    return this.user.role === 'admin';
  }
}
