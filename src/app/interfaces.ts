export interface IUserAuth {
  id: number;
  token: string;
  name: string;
  role: string;
  role_name: string;
}

export interface IUserInput {
  email: string;
  password: string;
}

export interface IUserDetail {
  id?: number;
  name: string;
  email: string;
  role: 'user' | 'admin';
  password?: string;
  created_at?: number;
  updated_at?: number;
}

export interface IAlert {
  type: string;
  message: string;
}
