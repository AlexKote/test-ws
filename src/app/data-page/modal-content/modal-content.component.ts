import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IUserDetail } from 'src/app/interfaces';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-modal-content',
  templateUrl: './modal-content.component.html',
  styleUrls: ['./modal-content.component.css'],
})
export class ModalContentComponent implements OnInit {
  private userId: number;
  public userForm: FormGroup;
  public title: string;

  @Input() set data(user: IUserDetail) {
    if (user === null) {
      this.userId = null;
      user = {
        name: '',
        email: '',
        password: '',
        role: 'user',
      };
      this.title = 'Add new User';
    } else {
      this.userId = user.id;
      this.title = 'Update this User';
    }

    this.userForm = new FormGroup({
      name: new FormControl(user.name, Validators.required),
      email: new FormControl(user.email, [
        Validators.required,
        Validators.email,
      ]),
      password: new FormControl(user.password, Validators.required),
      role: new FormControl(user.role, Validators.required),
    });
  }

  constructor(
    public activeModal: NgbActiveModal,
    private dataService: DataService
  ) {}

  ngOnInit(): void {}

  isAddUser(): boolean {
    return this.userId === null;
  }

  onSubmit(): void {
    if (this.isAddUser()) {
      this.dataService.addUser(this.userForm.value);
    } else {
      this.dataService.updateUser(this.userId, this.userForm.value);
    }
    this.onClose();
  }

  onClose(): void {
    this.activeModal.close('Close click');
  }
}
