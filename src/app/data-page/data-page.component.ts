import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth-page/auth.service';
import { Router } from '@angular/router';
import { DataService } from './data.service';
import { IUserDetail } from '../interfaces';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalContentComponent } from './modal-content/modal-content.component';
import { Subscription, BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-data-page',
  templateUrl: './data-page.component.html',
  styleUrls: ['./data-page.component.css'],
})
export class DataPageComponent implements OnInit, OnDestroy {
  private subcription: Subscription;
  public users$: BehaviorSubject<IUserDetail[]>;
  public users: IUserDetail[] = [];

  constructor(
    private authService: AuthService,
    private router: Router,
    private dataService: DataService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.dataService.loadUsers();
    this.users$ = this.dataService.getUsers();
  }

  onLogout(): void {
    this.authService.logout();
    this.router.navigate(['/auth']);
  }

  isAdmin(): boolean {
    return this.authService.isAdmin();
  }

  onDeleteUser(id: number): void {
    this.dataService.deleteUser(id);
  }

  openModal(user: IUserDetail): void {
    const modalRef = this.modalService.open(ModalContentComponent, {
      size: 'sm',
    });
    modalRef.componentInstance.data = user;
  }

  ngOnDestroy(): void {
    this.subcription.unsubscribe();
  }
}
