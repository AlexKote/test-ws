import { Injectable } from '@angular/core';
import { AuthService } from '../auth-page/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IUserDetail } from '../interfaces';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private api = 'http://dev.angulartest.digital-era.ru/api/user';
  private users: IUserDetail[];
  private users$ = new BehaviorSubject(null);

  constructor(private http: HttpClient, private authService: AuthService) {}

  loadUsers() {
    const url = `${this.api}?perPage=100&page=1`;
    const request = this.http.get(url, { headers: this.setHeaders() });
    const data = 'data';
    request.subscribe((res: Response) => {
      this.users = res[data];
      this.users$.next(this.users.slice());
    });
  }

  getUsers(): BehaviorSubject<IUserDetail[]> {
    return this.users$;
  }

  addUser(user: IUserDetail): void {
    const url = `${this.api}`;
    const request = this.http.post(url, user, { headers: this.setHeaders() });
    request.subscribe((newUser: IUserDetail) => {
      this.users.push(newUser);
      this.users$.next(this.users.slice());
    });
  }

  updateUser(idUser: number, user: IUserDetail): void {
    const url = `${this.api}/${idUser}`;
    const request = this.http.put(url, user, { headers: this.setHeaders() });
    request.subscribe((updatedUser: IUserDetail) => {
      const pos = this.users.findIndex(({ id }) => id === idUser);
      this.users[pos] = updatedUser;
      this.users$.next(this.users.slice());
    });
  }

  deleteUser(idUser: number): void {
    const url = `${this.api}/${idUser}`;
    const request = this.http.delete(url, { headers: this.setHeaders() });
    request.subscribe((res: Response) => {
      const pos = this.users.findIndex(({ id }) => id === idUser);
      this.users.splice(pos, 1);
      this.users$.next(this.users.slice());
    });
  }

  private setHeaders(): HttpHeaders {
    const myHeaders = new HttpHeaders().set(
      'Authorization',
      `Bearer ${this.authService.getToken()}`
    );
    return myHeaders;
  }
}
