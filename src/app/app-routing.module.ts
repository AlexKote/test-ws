import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthPageComponent } from './auth-page/auth-page.component';
import { DataPageComponent } from './data-page/data-page.component';
import { AuthGuard } from './auth-page/auth-guard.service';

const appRoutes: Routes = [
  { path: 'auth', component: AuthPageComponent },
  { path: 'data', component: DataPageComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: '/auth', pathMatch: 'full' },
  { path: '*', redirectTo: '/auth' },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class AppRoutingModule {}
